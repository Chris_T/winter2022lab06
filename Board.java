public class Board{
	
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	//Created 2 private die fields and a private closedTiles boolean
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		closedTiles = new boolean[12];
		//Create new die objects for the 2 die fields
		//Sets the boolean of closedTiles to a length of 12
	}
	
	public String toString(){
		String newString = "";
		//Create a newString to store tile numbers and X
		for (int i = 0; i < closedTiles.length; i++){
			if(closedTiles[i]==false){
				newString = newString + Integer.toString(i+1) + " ";
				//If the tile isn't closed, save the tile number as an integer
			}
			else{
				newString = newString + "X ";
				//If the tile is closed save the tile as "X"
			}
		}
		return newString;
		//Print the newString template when called
	}
	
	public boolean playATurn(){
		this.die1.roll();
		this.die2.roll();
		//Calls the roll method from the Die class on both die objects
		System.out.println(this.die1);
		System.out.println(this.die2);
		//Prints the value of each die
		int sum = this.die1.getPips() + this.die2.getPips();
		//Sums the total pips of the 2 dice
		if(closedTiles[sum-1] == false){
			//Checks if tile is closed
			closedTiles[sum-1] = true;
			//If not, close the tile
			System.out.println("Closing tile: " + (sum));
			return false;
		}
		else{
			System.out.println("The tile at this position is already shut");
			return true;
		}
		}
	}