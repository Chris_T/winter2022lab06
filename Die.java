import java.util.Random;

public class Die{
	
	private int pips;
	private Random random;
	//Created 2 private fields pips and random
	
	public Die(){
		this.pips = 1;
		this.random = new Random();
		//Sets pips' initial value to 1
		//Create a new Random object for the random field
	}
	
	public int getPips(){
		return this.pips;
		//Create a get method to allow other classes to access the pips fields
	}
	
	public void roll(){
		this.pips = random.nextInt(5)+1;
		//Generates a random number in the range of 1 to 6
	}
	
	public String toString(){
		return "The dice rolled a: " + this.pips;
		//Generates a random amount of pips for the die
	}
}