import java.util.Scanner;

public class ShutTheBox{
	
	public static void main(String[] args){
		
		System.out.println("Welcome to Shut The Box Game");
		//Welcomes the players
		boolean gameContinues = true;
		//Create a boolean to check if players want to continue playing
		Scanner scan = new Scanner(System.in);
		int player1Wins = 0;
		int player2Wins = 0;
		//Sets default wins to 0
		while(gameContinues == true){
			//Checks if players still wants to play
			Board board = new Board();
			//Creates a new board
			boolean	gameOver = false;
			//Creates a boolean to check if game is over or not
			while(gameOver != true){
				//As long as game is not over, run the loop
				System.out.println("Player 1's turn");
				System.out.println(board);
				//Prints the open/closed tiles with the toString defined in the Board class
				if(board.playATurn()==true){
					//Plays a turn for player 1, if tile was closed prior to player 1's turn, player 1 loses
					System.out.println("Player 2 wins");
					gameOver = true;
					//Ends game
					player2Wins ++;
					//Adds points to player 2
				}
				else{
					//If tile wasn't closed, proceeds to player 2's turn
					System.out.println("Player's 2 turn");
					System.out.println(board);
					if(board.playATurn()==true){
						//Checks if tile was closed prior to player 2's turn, if closed, player 2 loses
						System.out.println("Player 1 wins");
						player1Wins ++;
						//Adds points to player 1
						gameOver = true;
						//Ends game
					}
				}
			}
			System.out.println("Would you like to continue playing? y/n?");
			if(scan.nextLine().equals("n")){
				//Accepts player's answer and if players don't want to continue, proceed with next line of code
				gameContinues = false;
				//Stops games
			}
		}
		System.out.println("Player 1 won " + player1Wins + " games and player 2 won " + player2Wins + " games");
		//Prints the amount of wins each player got
	}
}